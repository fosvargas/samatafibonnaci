// Algoritmo de Fibonacci

function fibonacci() {

    let n1 = 0, n2 = 1;
    let result = [];
    var value = parseInt(document.getElementById('number_elements').value);
    
    for (let index = 0; index < value; index++) {

        let aux = n1;
        n1 = n2;
        n2 = aux + n2;
        result.push(n1);
        
    }

    document.getElementById("result_fibonacci").innerHTML = "<p>La serie es: " + (result + " ") + "</p>";

}
